/// <reference path="./../typings/passport-strategy/passport-strategy.d.ts" />
/// <reference path="./../typings/jwt-simple/jwt-simple.d.ts" />
"use strict";
const passport = require('passport-strategy');
const JwtSimple = require('jwt-simple');
class RobotStrategy extends passport.Strategy {
    constructor(options, log = null) {
        super();
        if (!options || options.secret == '') {
            throw new TypeError('Options did not specify a key.');
        }
        this._options = options;
        this._log = log;
    }
    static get name() {
        return 'robot';
    }
    authenticate(req, options) {
        options = options || {};
        let authArgs = options;
        let jwtToken = req.headers['Access-Token'];
        let secureUser = null;
        let self = this;
        let redirectUrl = this._options.iam_url +
            (authArgs.requireNonHuman ? '/direct' : '');
        if (!jwtToken) {
            if (this._options.iam_url && this._options.iam_url != '' && authArgs.redirectOnInvalidToken === true) {
                return this.redirect(redirectUrl);
            }
            return this.fail({ message: 'Bad request, missing header "auth-token".' }, 401);
        }
        try {
            secureUser = JwtSimple.decode(jwtToken, this._options.secret, false, this._options.algorithm);
        }
        catch (error) {
            if (this._log && this._log.error) {
                this._log.error(error, 'authenticate', module.filename);
            }
            if (this._options.iam_url && this._options.iam_url != '' && authArgs.redirectOnInvalidToken === true) {
                return this.redirect(redirectUrl);
            }
            return this.error(error);
        }
        if (!secureUser) {
            return this.fail({ message: 'The passed authorization token was malformed.' }, 401);
        }
        if (authArgs.requireGlobalAdmin === true && secureUser.global_admin != true) {
            return this.fail({ message: 'The requested operation is only avalible for global-admins.' }, 403);
        }
        if (authArgs.requireNonHuman === true && secureUser.profile.human === true) {
            return this.fail({ message: 'The requested operation is only avalible for non-human users.' }, 403);
        }
        if (authArgs.groups) {
            if (typeof (authArgs.groups) != 'object') {
                let error = new Error('Expected options.groups to be an object.');
                if (this._log && this._log.error) {
                    this._log.error(error, 'authenticate', module.filename);
                }
                return this.error(error);
            }
            for (let group in authArgs.groups) {
                let requiredRole = authArgs.groups[group];
                if (!secureUser.roles[group]) {
                    return this.fail({ message: 'Authorization failed, this operation requires membership in group ' + group }, 403);
                }
                let actualRole = secureUser.roles[group];
                if (requiredRole != actualRole && actualRole != 'admin') {
                    if (requiredRole == 'read' && actualRole == 'write') {
                        continue;
                    }
                    return this.fail({ message: 'Authorization failed, this operation requires role ' + requiredRole + ' in group ' + group }, 403);
                }
            }
        }
        this.success(secureUser, null);
    }
}
exports.RobotStrategy = RobotStrategy;
//# sourceMappingURL=index.js.map