/// <reference path="../typings/passport-strategy/passport-strategy.d.ts" />
/// <reference path="../typings/jwt-simple/jwt-simple.d.ts" />
import * as passport from 'passport-strategy';
import { Request } from 'express';
export interface IRobotStrategyOptions {
    secret: string;
    iam_url: string;
    algorithm: string;
}
export interface IRobotStrategyLog {
    error: (error: Error, method: string, filename: string) => void;
    info: (message: string, method: string, filename: string) => void;
}
export interface IRobotStrategyAuthOptions {
    requireNonHuman?: boolean;
    requireGlobalAdmin?: boolean;
    groups?: any;
    redirectOnInvalidToken?: boolean;
}
export declare type MemberRole = 'read' | 'write' | 'admin' | 'none';
export interface IRoboticNonSecureProfile extends IRoboticSecureProfile {
    access_token: string;
}
export interface IRoboticSecureProfile {
    auth: {
        sso_id: string;
        client_key?: string;
        client_secret?: string;
    };
    name?: string;
    profile?: {
        firstname: string;
        lastname: string;
        email: string;
        location: string;
        fullname: string;
        human: boolean;
    };
    global_admin?: boolean;
    roles: any;
    issued: number;
    expires: number;
}
export declare class RobotStrategy extends passport.Strategy {
    private _options;
    private _log;
    constructor(options: IRobotStrategyOptions, log?: IRobotStrategyLog);
    static name: string;
    authenticate(req: Request, options?: any): any;
}
