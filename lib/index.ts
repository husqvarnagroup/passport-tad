/// <reference path="./../typings/passport-strategy/passport-strategy.d.ts" />
/// <reference path="./../typings/jwt-simple/jwt-simple.d.ts" />

import * as passport from 'passport-strategy';
import * as JwtSimple from 'jwt-simple';
import {Request} from 'express';

export interface IRobotStrategyOptions {
    secret : string;
    iam_url : string;
    algorithm : string;
}

export interface IRobotStrategyLog {
    error : (error : Error, method : string, filename : string) => void;
    info : (message : string, method : string, filename : string) => void;
}

export interface IRobotStrategyAuthOptions {
    requireNonHuman? : boolean;
    requireGlobalAdmin? : boolean;
    groups? : any;
    redirectOnInvalidToken? : boolean;
}

export type MemberRole = 'read' | 'write' | 'admin' | 'none';


export interface IRoboticNonSecureProfile extends IRoboticSecureProfile {
    access_token : string;
}

export interface IRoboticSecureProfile {
    auth: {
        sso_id: string;
        client_key?: string;
        client_secret?: string;
    };
    name?: string;
    profile?: {
        firstname: string;
        lastname: string;
        email: string;
        location: string;
        fullname: string;
        human: boolean
    };
    global_admin?: boolean;
    roles : any;
    issued : number;
    expires: number;
}

export class RobotStrategy extends passport.Strategy{
    private _options : IRobotStrategyOptions;
    private _log : IRobotStrategyLog;

    constructor(options : IRobotStrategyOptions, log : IRobotStrategyLog = null) {
        super();

        if(!options || options.secret == '') {
            throw new TypeError('Options did not specify a key.');
        }
        
        this._options = options;
        this._log = log;
    }

    public static get name() {
        return 'robot';
    }

    authenticate (req : Request, options? : any) : any {
        options = options || {};
        let authArgs : IRobotStrategyAuthOptions = options as IRobotStrategyAuthOptions;

        let jwtToken = req.headers['Access-Token'];
        let secureUser : IRoboticSecureProfile = null;
        let self : RobotStrategy = this;

        let redirectUrl = this._options.iam_url +
                    (authArgs.requireNonHuman ? '/direct' : '');

        if(!jwtToken) {
            if(this._options.iam_url && this._options.iam_url != '' && authArgs.redirectOnInvalidToken === true) {
                
                return this.redirect(redirectUrl);
            }

            return this.fail({message : 'Bad request, missing header "auth-token".'}, 401);
        }
        
        try {
            secureUser = JwtSimple.decode(jwtToken, this._options.secret,false, this._options.algorithm);
        } catch(error) {
            if(this._log && this._log.error) {
                this._log.error(error, 'authenticate', module.filename);
            }

            if(this._options.iam_url && this._options.iam_url != '' && authArgs.redirectOnInvalidToken === true) {
                return this.redirect(redirectUrl);
            }

            return this.error(error);
        }

        if(!secureUser) {
            return this.fail({message : 'The passed authorization token was malformed.'}, 401);
        }

        if(authArgs.requireGlobalAdmin === true && secureUser.global_admin != true) {
            return this.fail({message : 'The requested operation is only avalible for global-admins.'}, 403);
        }

        if(authArgs.requireNonHuman === true && secureUser.profile.human === true) {
            return this.fail({message : 'The requested operation is only avalible for non-human users.'}, 403);
        }
        
        if(authArgs.groups) {
            if(typeof(authArgs.groups) != 'object') {
                let error = new Error('Expected options.groups to be an object.');

                if(this._log && this._log.error) {
                    this._log.error(error,'authenticate', module.filename);
                }

                return this.error(error);
            }

            for(let group in authArgs.groups) {
                let requiredRole = authArgs.groups[group];

                if(!secureUser.roles[group]) {
                    return this.fail({message : 'Authorization failed, this operation requires membership in group ' + group}, 403);
                }

                let actualRole = secureUser.roles[group];

                if(requiredRole != actualRole && actualRole != 'admin') {
                    if(requiredRole == 'read' && actualRole == 'write'){
                        continue;
                    }

                    return this.fail({message : 'Authorization failed, this operation requires role ' + requiredRole + ' in group ' + group}, 403);
                }
            }
        }

        this.success(secureUser, null);
    }
}